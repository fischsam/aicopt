'''
Created on 02.06.2020

@author: Samuel
'''


"""
Algorithm 3. existsSubset(node,set)
1:if(node.last_flag==true) then
2:    return  true;
3:end if
4:if(notword.existsCurrentElement)then
5:    return  false;
6:end if
7:found = false;
8:if(nodehas child labeledword.currentElement)then
9:    nextNode= child ofnodelabeledword.currentElement;
10:    found= existsSubset(nextNode,word.gotoNextElement);
11:end if
12:if(!found)then
13:    return existsSubset(node,word.gotoNextElement);
14:else
15:    return  true;16:end if
"""


from datrie import BaseTrie, BaseState, BaseIterator

from settriec import existsSubset

def print_state(state):
    it = BaseIterator(state)
    strr = ''
    while it.next():
        strr += it.key() + ","
    print(strr)

def existsSubset3(trie, setarr, trieState=None):
    
    if trieState is None:
        trieState = BaseState(trie)
        
    if trieState.is_leaf():
        return True
    if not len(setarr):
        return False
    
    #print(setarr, end='|')
    #print_state(trieState)
    found = False
    trieState2 = BaseState(trie)
    trieState.copy_to(trieState2)
    hasAdmissibleChild = trieState2.walk(setarr[0])
    if hasAdmissibleChild:
        found = existsSubset(trie, setarr[1:], trieState2)
    if not found:
        return existsSubset(trie, setarr[1:], trieState)
    else:
        return True

def existsSubset(trie, setarr, trieState=None):
    
    if trieState is None:
        trieState = BaseState(trie)
        
    trieState2 = BaseState(trie)
    trieState.copy_to(trieState2)
    for i, elem in enumerate(setarr):
        if trieState2.walk(elem):
            if trieState2.is_leaf() or existsSubset(trie, setarr[i:], trieState2): 
                return True
            trieState.copy_to(trieState2)
    return False
    


def test():
    trie = BaseTrie('123456780')
    trie['123'] = 3
    trie['678'] = 8
    trie['1345'] = 5
    print(existsSubset((trie), chr(3)))
    print(existsSubset((trie), '134678'), existsSubset2((trie), '134678'), True)
    print("done")
    print(existsSubset((trie), '45678'), existsSubset2((trie), '45678'),True)
    print(existsSubset((trie), '1234'), existsSubset2((trie), '1234'), True)
    print(existsSubset((trie), '679'), existsSubset2((trie), '679'), False)
    print(existsSubset((trie), '13456'), existsSubset2((trie), '13456'), True)

if __name__ == '__main__':
    test()